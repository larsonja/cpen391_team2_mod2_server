var express = require("express"),
    assert = require("assert");

module.exports = function(dbHandle) {
  assert(dbHandle);

  var api = express.Router(),
      coll = "counts";

  // CORS Support
  api.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type");

    next();
  }); 

  // GET Request Routes
  api.get("/:name", function(req, res) {
    req.params.name
    dbHandle.open(function(err, db) {
      if (err) {
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Error Connecting to Database!");
        console.log("Error Connecting to Database!");
        return;
      }

      var filter = { name: req.params.name },
          update = { $inc: { count: 1 } },
          options = {
            upsert: true,
            projection: {
              _id: 0,
              name: 1,
              count: 1
            }
          };
      db.collection(coll).findOneAndUpdate(filter, update, options)
        .then(function success(original) {
          if (!original.value) {
            // Original document did not exist, our operation was an upsert
            res.json({
              name: req.params.name,
              count: 0
            });
          } else {
            // Return the original document before increment
            res.json(original.value);
          }

          return null;

        }, function error(err) {
          res.writeHead(500, {"Content-Type": "text/plain"});
          res.end("Error Retrieving Count!");
          console.log("Error Retrieving Count!");
          return null;

        }).then(function done() {
          db.close();
        });
    });
  });

  return api;
};
