var express = require("express"),
    morgan = require("morgan"),
    mongoWrapper = require("./my_mongo_connector/mongoWrapper"),
    countApi = require("./countApi");
    api = require("./api");

var app = express(),
    port = process.argv[2] || process.env.PORT || 80,
    dbParams = {
                 hostname: "ds023088.mlab.com",
                 port: 23088,
                 user: "my_mongo_admin",
                 password: "mymongopass"
               },
    model = {
              dbHandle: mongoWrapper.client(dbParams),
              "COLL_PREFIX": "geocache_",
              "DE2_RES_PATH": "static/resources/"
            };

app.use(morgan("dev"));

app.use("/count", countApi(mongoWrapper.client(dbParams)));
app.use("/", api(model));

app.listen(port);
console.log("Listening on port " + port + "...");
