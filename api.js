var express = require("express"),
    bodyParser = require("body-parser"),
    assert = require("assert"),
    path = require("path"),
    fs = require("fs");

module.exports = function(model) {
  assert(model.COLL_PREFIX);
  assert(model.DE2_RES_PATH);
  assert(model.dbHandle);

  var COLL_PREFIX = model.COLL_PREFIX,
      DE2_RES_PATH = model.DE2_RES_PATH,
      dbHandle = model.dbHandle,
      api = express.Router(),
      packagesMetaData,
      debug = false;

  api.use("/de2", express.static(__dirname + "/static/resources"));
  api.use(bodyParser.json());

  // GET Request Routes
  api.get("/de2/:dirName*", function(req, res) {
    if (debug) {
      console.log("req.url: " + req.url);
      console.log("req.params: " + JSON.stringify(req.params));
      console.log("req.query: " + JSON.stringify(req.query));
      console.log("req.params.dirName: " + req.params.dirName);
    }

    fs.readdir(DE2_RES_PATH + req.params.dirName, function(err, filesList) {
      if (err) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Error retrieving files!");
        console.log("Error retrieving files!");
        return;
      }
      var resJson = {};
      resJson["files"] = filesList.filter(function(e, i, arr) {
        if (e == "MAP_FULL.TXT")
          return false;
        else
          return true;
      });;
      res.writeHead(200, {"Content-Type": "application/json"});
      res.end(JSON.stringify(resJson));
    });
  });

  api.get("/packages", function(req, res) {
    if (!req.query.user) {
      res.writeHead(400, {"Content-Type": "text/plain"});
      res.end("No User Specified To Retrieve Packages For!");
      console.log("No User Specified To Retrieve Packages For!");
      return;
    }

    var coll = COLL_PREFIX + "users";
    dbHandle.find(coll, { email: req.query.user },
                  { progress: 1, _id: 0}, function(err, progress) {
      if (err || progress.length != 1) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Error retrieving user information!");
        console.log("Error retrieving user information!");
        return;
      }
      progress = progress[0].progress;

      fs.readdir(DE2_RES_PATH, function(err, filesList) {
        if (err) {
          res.writeHead(500, {"Content-Type": "text/plain"});
          res.end("Error retrieving files!");
          console.log("Error retrieving files!");
          return;
        }

        var cpColl = COLL_PREFIX + "cache_packages";
        if (!packagesMetaData) {
          dbHandle.find(cpColl, {}, { _id: 0 }, function(err, results) {
            if (err) {
              res.writeHead(500, {"Content-Type": "text/plain"});
              res.end("Error packages meta-data!");
              console.log("Error packages meta-data!");
              return;
            }

            packagesMetaData = {};
            results.forEach(function(e, i, arr) {
              packagesMetaData[e.name] = {};
              for (var prop in e) {
                if (prop != "name")
                  packagesMetaData[e.name][prop] = e[prop]
              }
            });
            handlePackagesUserQuery(packagesMetaData, progress,
                                    res, filesList);
          });
          return;
        }
        handlePackagesUserQuery(packagesMetaData, progress,
                                res, filesList);
      });
    });
  });

  api.get("/users", function(req, res) {
    var coll = COLL_PREFIX + "users";

    // Response with all users
    var projection = {
                       _id: 0,
                       email: 1
                     };
    dbHandle.find(coll, null, projection, function(err, docs) {
      if (err) {
        // Error Quering Database
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Error Querying Database for "
                + coll + " collection!");
        console.log("Error Querying Database for "
                    + coll + " collection!");
        return;
      }

      var userEmails = docs.map(function(e, i, arr) {
        return e.email;
      });
      res.writeHead(200, {"Content-Type": "application/json"});
      res.end(JSON.stringify(userEmails));
    });
  });

  api.get("*", function(req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.write("Thank you for sending GET request!\n\n"
              + "Please request for " + req.get("host") + "/users"
              + " for the users list\n\n");
    res.write("To retrieve package information, "
              + "please append a query string of the format:\n"
              + "?user=useremail@example.com after the /packages url\n"
              + "ex: " + req.get("host")
              + "/packages?user=useremail@example.com");
    res.end();
  });

  // POST Request Routes
  api.post("/users", function(req, res) {
    if (req.body === undefined) {
      res.writeHead(400, {"Content-Type": "text/plain"});
      res.end("Bad POST Request! Undefined request body.");
      console.log("Bad POST Request! Undefined request body.");
      return;
    } else if (req.body.email === undefined) {
      res.writeHead(400, {"Content-Type": "text/plain"});
      res.end("Bad POST Request! Undefined user email.");
      console.log("Bad POST Request! Undefined user email.");
      return;
    }

    var coll = COLL_PREFIX + "users";
    var updateObj = {};
    var msg = "updated";
    for (var prop in req.body) {
      if (prop == "email")
        updateObj[prop] = req.body[prop];
      else
        updateObj["progress." + prop] = req.body[prop];
    }
    if (Object.keys(updateObj).length == 1) {
      updateObj.progress = {};
      msg = "created";
    }

    dbHandle.updateOne(coll, "email", { $set: updateObj },
                       function(err, results) {
      if (err) {
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.end("Error updating database for user!"); 
        console.log("Error updating database for user!"); 
        return;
      }
  
      res.writeHead(200, {"Content-Type": "text/plain"});
      res.end("User successfully " + msg + "!");
    });
  });

  api.post("*", function(req, res) {
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("Thank you for sending POST request!\n");
  });

  // DELETE Request Routes
  api.delete("/users", function(req, res) {
    if (Object.keys(req.query).length == 0 || !req.query.email) {
      res.writeHead(400, {"Content-Type": "text/plain"});
      res.end("Bad DELETE Request! User Email not Specified!");
      console.log("Bad DELETE Request! User Email not Specified!");
      return;
    }

    var coll = COLL_PREFIX + "users";
    dbHandle.deleteOne(coll, { email: req.query.email },
                       function(err, results) {
      if (err) {
        res.writeHead(500, {"Content-Type": "text/plain"});
        res.end("Error delete user '" + req.query.email
                + "' from database!");
        console.log("Error delete user '"
                    + req.query.email + "' from database!");
        return;
      }

      res.writeHead(200, {"Content-Type": "text/plain"});
      res.end("User successfully deleted!");
    });
  });

  return api;
}

function handlePackagesUserQuery(metaData, progress, res, filesList) {
  if (!metaData) {
    res.writeHead(500, {"Content-Type": "text/plain"});
    res.end("Internal Server Error!"
            + " Could not retrieve package metadata!");
    console.log("Internal Server Error!"
                + " Could not retrieve package metadata!");
    return;
  }

        var resJson = {};
        resJson["packages"] = filesList.filter(function(e, i, arr) {
          // Return only the name of directories, ie: packages
          if (path.extname(e) == "") 
            return true;

          return false;

        }).map(function(packName, i, arr) {
          return {
            name: packName,
            description: metaData[packName].description,
            progress: (progress[packName])?
                         parseInt(progress[packName]) : 0,
            numCaches: parseInt(metaData[packName].numCaches),
            lat: parseFloat(metaData[packName].coordinates[0]),
            lon: parseFloat(metaData[packName].coordinates[1])
          };
        });

        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(JSON.stringify(resJson));
}
