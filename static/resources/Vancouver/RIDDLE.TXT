What goes up but never comes down?~Age~Birds~Balloon~Elevators
What's black when you get it, red when you use it, and white when you're done with it?~Charcoal~Lobster~Bullet~Newspaper
I'm tall when I'm young and I'm short when I'm old. What am I?~A Candle~A Tree~A Human~A Shadow